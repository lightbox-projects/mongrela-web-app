/* eslint-disable */
import notFoundPage from '../pages/NotFound.vue'

import { DOMAIN_TITLE } from '../.env'

export const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import('@/pages/landing-page/index.vue'),
    meta: { title: `${DOMAIN_TITLE} | Landing` }
  },
  {
    path: '/privacy-policy',
    name: 'privacy-policy',
    component: () => import('@/pages/privacy-policy.vue'),
    meta: { title: `${DOMAIN_TITLE} | Privacy Policy` }
  },
  {
    path: '*',
    component: notFoundPage,
    meta: { title: `${DOMAIN_TITLE} | not found` }
  }
]
